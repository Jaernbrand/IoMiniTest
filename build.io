#!/usr/bin/env io

build := Object clone do (

	/**
	 * Auxiliary method to check whether the current platform is a Windows 
	 * platform.
	 */
	isWindows := method(
		platform := System platform 
		(platform containsSeq("Windows") or 
			platform == "mingw" or 
			platform == "cygwin")
	)
	
	SOURCE_DIR := "MiniTest"
	MAIN_SCRIPT := "MiniTest.io"

	EXEC_NAME := "IoMiniTest"
	(isWindows) ifTrue (EXEC_NAME := EXEC_NAME .. ".bat")
	INSTALL_EXEC_DIR := if(isWindows, 
		"#{System installPrefix}\\bin", 
		"#{System installPrefix}/bin") interpolate
	INSTALL_EXEC_PATH := if(isWindows, 
		"#{INSTALL_EXEC_DIR}\\#{EXEC_NAME}",
		"#{INSTALL_EXEC_DIR}/#{EXEC_NAME}") interpolate

	INSTALL_PACKAGE_DIR := if (isWindows, 
		"#{System ioPath}\\packages",
		"#{System ioPath}/packages") interpolate
	INSTALL_PATH := if (isWindows,
		"#{INSTALL_PACKAGE_DIR}\\#{SOURCE_DIR}",
		"#{INSTALL_PACKAGE_DIR}/#{SOURCE_DIR}") interpolate

	MAIN_INSTALL_PATH := if (isWindows, 
		"#{INSTALL_PATH}\\#{MAIN_SCRIPT}", 
		"#{INSTALL_PATH}/#{MAIN_SCRIPT}") interpolate

	/**
	 * Installs the package for the Io installation used to execute this script.
	 * A executable script that in turn executes the main script is also added 
	 * in the directory where the Io installation's binary is installed, to 
	 * allow direct execution of the package.
	 */
	install := method(
		Directory createIfAbsent(INSTALL_PACKAGE_DIR)
		
		copyDir := method(dirPath, 
			dir := Directory with(dirPath)
			dir directories foreach(subdir, 
				copyDir("#{dirPath}/#{subdir name}" interpolate))
			
			destPath := "#{INSTALL_PACKAGE_DIR}/#{dirPath}" interpolate
			destDir := Directory with(destPath)
			destDir createIfAbsent

			dir files foreach(file, 
				file copyToPath("#{destPath}/#{file name}" interpolate)
				file close)
		)

		copyDir(SOURCE_DIR)

		chmodExec := method(filepath,
			retVal := System system("chmod +x #{filepath}" interpolate)
			if (retVal != 0, 
				Exception raise(("Could not change mode to executable for " ..
					"file #{filepath}") interpolate)
			)
		)

		writeExecScript := method(path, content, 
			script := File with(path)
			if (File exists(path), 
				script remove)
			script open
			script write(content)
			script close
		)

		installWindowsExec := block(
			content := ("SET IOIMPORT=#{INSTALL_PACKAGE_DIR};%IOIMPORT%" .. 
				"\r\nio #{MAIN_INSTALL_PATH} %*") interpolate
			writeExecScript(INSTALL_EXEC_PATH, content)
		)

		installNixExec := block(
			content := ("#!/bin/bash\n" .. 
				"export IOIMPORT=#{INSTALL_PACKAGE_DIR}:$IOIMPORT\n" .. 
				"#{MAIN_INSTALL_PATH} \"$@\"") interpolate
			writeExecScript(INSTALL_EXEC_PATH, content)
			chmodExec(INSTALL_EXEC_PATH)
			chmodExec(MAIN_INSTALL_PATH)
		)

		if (isWindows) then (
			installWindowsExec call
		) else (
			installNixExec call
		)
		"Successfully installed at #{INSTALL_PATH}" interpolate println
		"Executable script created at #{INSTALL_EXEC_PATH}" interpolate println
		"Done"
	)

	/**
	 * Uninstalls the package from the Io installation used to run this script.
	 * The script used to execute the main script is also removed. 
	 */
	uninstall := method(
		if (Directory exists(INSTALL_PATH)) then (
			Directory with(INSTALL_PATH) remove
			"Successfully removed #{INSTALL_PATH}" interpolate println
		)

		if (isWindows) then (
			retVal := System system("del /F #{INSTALL_EXEC_PATH}" interpolate)
		) else (
			retVal := System system("rm -f #{INSTALL_EXEC_PATH}" interpolate)
		)
		if (retVal != 0) then (
			msg := ("Failed to remove script at #{INSTALL_EXEC_PATH}. " ..
				"Returned value '#{retVal}'") interpolate
			Exception raise(msg)
		) else (
			"Successfully removed #{INSTALL_EXEC_PATH}" interpolate println
		)
		"Done" println 
	)

	/**
	 * Runs all test.
	 */
	test := method(
		Importer addSearchPath(SOURCE_DIR)
		MiniTest run(list("test"))
	)

)

args := System args slice(1) 
if (args size == 1) then (
	build perform(args at (0)) 
) else (
	"Supported arguments" println
	"* install : Installs the package for the Io installation used to " print 
	"execute this script." println
	"* uninstall :  Uninstalls the package from the Io installation used " print 
	"to run this script." println
	"* test : Runs all tests." println
)


