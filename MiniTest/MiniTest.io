#!/usr/bin/env io

/**
 * The entry point for running tests via IoMiniTest.
 */
MiniTest := Object clone do(
	
	VERSION := "1.0.0"

	defaultLoader := method(
		MiniTestLoader clone)

	defaultRunner := method(
		MiniTestRunner clone)

	/**
	 * Run MiniTest with the given list of arguments.
	 */
	run := method(args,
		(args == nil) ifTrue (args := list())
		loader := self defaultLoader()
		suite := self parseArgs(args, loader)

		runner := self defaultRunner()
		runner run(suite))

	parseArgs := method(args, loader,
		if (args size == 0) then (
			suite := loader loadTestsFromDir(loader DEFAULT_TEST_PATTERN, 
				loader DEFAULT_SEARCH_DIR, 
				loader DEFAULT_TEST_SLOT_PATTERN)
		) elseif (args size == 1 and self isHelpArg(args at(0)) ) then (
			self printHelp
			System exit
		) elseif (args size == 1 and self isVersionArg(args at(0)) ) then (
			self printVersion
			System exit
		) elseif (args size == 1 and Directory exists(args at(0)) ) then (
			suite := loader loadTestsFromDir(loader DEFAULT_TEST_PATTERN, 
				args at(0), 
				loader DEFAULT_TEST_SLOT_PATTERN)
		) elseif (args size > 0) then (
			suite := loader loadTestsFromPaths(args, 
				loader DEFAULT_TEST_SLOT_PATTERN)
		) else (
			Exception raise("Unknown argument")
		)
		suite)

	isVersionArg := method(arg,
		arg == "--version")

	printVersion := method(
		"MiniTest version: #{self VERSION}" interpolate println)

	isHelpArg := method(arg,
		arg == "-h" or arg == "--help")

	printHelp := method(
		"""
MiniTest [directory | tests | -h | --help | --version]

* directory : Run all tests in the specified directory and all tests in its 
subdirectories.

* tests : Run all tests in the specified test case. The test case is expected 
to have the same name as the file it resides in, excluding the file 
extension (.io).

* -h, --help : Print help.

* --version : Print the current version.
""" println)
)
	
if (isLaunchScript) then (
	MiniTest run( System args slice(1) ))

