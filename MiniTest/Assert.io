
/**
 * Contains various assertions methods. 
 */
Assert := Object clone do (

	/**
	 * Asserts the given boolean. Raises an AssertionException with the given
	 * message if bool is false. 
	 */
	assert := method(bool, msg,
		if (bool, return)
		err := AssertionError with(msg)
		AssertionException raise(err))

	/**
	 * Asserts that expected is equal to actual. Raises an AssertionException
	 * with an optional message if the assertion fails.
	 */
	assertEquals := method(expected, actual, msg,
		(msg isNil) ifTrue (msg := "'#{expected}' != '#{actual}'" interpolate)
		self assert(expected == actual, msg))

	/**
	 * Asserts that expected is not equal to actual. Raises an AssertionException
	 * with an optional message if the assertion fails.
	 */
	assertNotEquals := method(expected, actual, msg,
		(msg isNil) ifTrue (msg := "'#{expected}' == '#{actual}'" interpolate)
		self assert(expected != actual, msg))

	/**
	 * Asserts that expected is equal to actual, within the given delta. 
	 * Raises an AssertionException with an optional message if the assertion 
	 * fails.
	 */
	assertAlmostEquals := method(expected, actual, delta, msg,
		(msg isNil) ifTrue (msg := "'#{expected}' != '#{actual}' within delta '#{delta}'" interpolate)
		diff := (expected - actual) abs
		self assert(diff <= delta, msg))

	/**
	 * Asserts that expected is not equal to actual, withing the given delta. 
	 * Raises an AssertionException with an optional message if the assertion 
	 * fails.
	 */
	assertNotAlmostEquals := method(expected, actual, delta, msg,
		(msg isNil) ifTrue (msg := "'#{expected}' == '#{actual}' within delta '#{delta}'" interpolate)
		diff := (expected - actual) abs
		self assert(diff > delta, msg))

	/**
	 * Asserts that the given object is nil. Raises an AssertionException with
	 * an optional custom message if the assertion fails.
	 */
	assertNil := method(obj, msg,
		(msg isNil) ifTrue (msg := "'#{obj}' != 'nil'" interpolate)
		self assert(obj isNil, msg))

	/**
	 * Asserts that the given object is not nil. Raises an AssertionException 
	 * with an optional custom message if the assertion fails.
	 */
	assertNotNil := method(obj, msg,
		(msg isNil) ifTrue (msg := "'#{obj}' == 'nil'" interpolate)
		self assert(obj isNil not, msg))

	/**
	 * Asserts that bool is true. Raises an AssertionException if the assertion
	 * fails. An optional message for the exception may be passed as second 
	 * argument.
	 */
	assertTrue := method(bool, msg,
		(msg isNil) ifTrue (msg := "'#{bool}' != 'true'" interpolate)
		self assert(bool, msg))

	/**
	 * Asserts that bool is false. Raises an AssertionException if the assertion
	 * fails. An optional message for the exception may be passed as second 
	 * argument.
	 */
	assertFalse := method(bool, msg,
		(msg isNil) ifTrue (msg := "'#{bool}' != 'false'" interpolate)
		self assert(bool not, msg))

	/**
	 * Fail and raise an AssertionException. An optional error message may be
	 * passed as argument.
	 */
	fail := method(msg, 
		(msg isNil) ifTrue (msg := "Fail (no reason given)")
		self assert(false, msg))
)

/**
 * Contains assertion error information.
 */
AssertionError := Error clone

/**
 * Indicate that an assertion failed.
 */
AssertionException := Exception clone

