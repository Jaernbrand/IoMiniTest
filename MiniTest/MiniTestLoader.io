
// Required to be able to find tests based on regex pattern
Regex 

/**
 * Loads tests from their protos and constructs suites ready to run.
 */
MiniTestLoader := Object clone do(

	DEFAULT_SEARCH_DIR := "."
	DEFAULT_TEST_PATTERN := "^Test"
	DEFAULT_TEST_SLOT_PATTERN := "^test"
	TEST_FILE_EXTENSION := "io"

	/**
	 * Loads all tests found in the specified directory. Tests must match the
	 * given test slot pattern and the test protos have to reside in files 
	 * matching the given file pattern.
	 */
	loadTestsFromDir := method(filePattern, dir, testSlotPattern,
		fileNames := self discover(filePattern, dir)
		self loadTests(fileNames, testSlotPattern))

	/**
	 * Load all tests in the specified file paths. The file paths are expected
	 * to point to test protos. Test methods have to match the test slot 
	 * pattern.
	 */
	loadTestsFromPaths := method(filePaths, testSlotPattern,
		fileNames := self fileNames(filePaths)
		self directories(filePaths) foreach(dir, Importer addSearchPath(dir))
		self loadTests(fileNames, testSlotPattern))

	/**
	 * Load all tests in the specified file names, no path to the file is 
	 * expected. Test methods have to match the test slot pattern.
	 */
	loadTests := method(fileNames, testSlotPattern,
		suite := MiniTestSuite clone
		subsuites := fileNames map(file, 
			testSlots := self testMethods(file, testSlotPattern)
			self loadTestSlots(file, testSlots))
		suite addTestSeq(subsuites)
		suite)

	/**
	 * Loads the test slots from the given file.
	 */
	loadTestSlots := method(fileName, testSlots,
		suite := self initTestCases(fileName, testSlots)
		suite)

	/**
	 * Find  all test cases that match the given pattern and are located in
	 * the specified directory or any of its subdirectories.
	 */
	discover := method(pattern, dir,
		tests := self findTests(dir, pattern)
		fileNames := self fileNames(tests)

		self directories(tests) foreach(dir, Importer addSearchPath(dir))
		fileNames)

	/**
	 * Returns a list of all filenames of the given list of paths. 
	 */
	fileNames := method (paths,
		paths map(elem, elem fileName))

	/**
	 * Returns the directory component of all paths as a list of strings.
	 */
	directories := method (paths,
		paths map(elem, elem pathComponent) unique)

	/**
	 * Find all test cases in the given dir path based on the given pattern.
	 * Subdirectories to the specified directory will also be searched for test cases.
	 */
	findTests := method(dirPath, pattern,
		files := List clone
		dirs := List clone
		dirs append(Directory clone directoryNamed(dirPath))

		filePattern := self getTestPattern(pattern)
		while(dirs size > 0,
			currDir := dirs pop
			currDir fileNames foreach(elem, 
				if(elem hasMatchOfRegex(filePattern), 
					files append("#{currDir path}/#{elem}" interpolate)))
			dirs appendSeq(currDir directories))
		files)

	/**
	 * Constructs the pattern used to find source files containing test cases
	 * based on the given naming pattern.
	 */
	getTestPattern := method(pattern,
		"#{pattern}.*[.]#{self TEST_FILE_EXTENSION}$" interpolate)


	/**
	 * Returns all slots in testCase matching the specified slot pattern. 
	 * The slots are returned in a list as strings.
	 */
	testMethods := method(testCase, slotPattern,
		slots := doString(testCase) slotNames
		testSlots := slots select(currSlot, 
			currSlot hasMatchOfRegex(slotPattern))
		testSlots)


	/**
	 * Initialize the tests specified in testSlots and residing in the testCase
	 * proto. Returns a suite containing initialized clones of testCase.
	 */
	initTestCases := method(testCase, testSlots,
		suite := MiniTestSuite clone
		cases := testSlots map(currSlot, 
			currTestCase := doString(testCase) clone
			currTestCase setSelector(currSlot)
			currTestCase)
		suite addTestSeq(cases)
		suite setName(testCase)
		suite)
)
