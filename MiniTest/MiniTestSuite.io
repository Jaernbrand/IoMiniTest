
/**
 * Composite of other suites and test cases.
 */
MiniTestSuite := Object clone do (

	init := method(
		self tests := List clone
		self newSlot("name"))

	/**
	 * Add testCase to the test list.
	 */
	addTest := method(testCase,
		self tests append(testCase))

	/**
	 * Add the elements in testSeq to the tests list.
	 */
	addTestSeq := method(testSeq,
		self tests appendSeq(testSeq))

	/**
	 * Returns the number of tests in the suite.
	 */
	testCount := method(
		count := 0
		self tests foreach(test, 
			count = count + test testCount)
		count)

	/**
	 * Run all tests contained in the suite.
	 */
	run := method(result,
		testProto := tests at(0) proto
		testProto ?preSetUp
		self tests foreach(test, test run(result))
		testProto ?postTearDown
		result)
)
