
/**
 * Runs tests and reports the result.
 */
MiniTestRunner := Object clone do (

	/**
	 * Returns a clone of the defaultTestResultProto.
	 */
	defaultTestResult := method(
		self defaultTestResultProto clone)

	/**
	 * The default proto for containing test results.
	 */
	defaultTestResultProto := method(MiniTestResult)

	/**
	 * Run all tests in the supplied test suite and print the result.
	 */
	run := method(suite,
		result := self defaultTestResult

		result startTestRun
		suite run(result)
		result stopTestRun
		self report(result)
		result)

	/**
	 * Print report based on the supplied result.
	 */
	report := method(result,
		"\n" println
		self reportFailures(result)
		self reportErrors(result)
		self reportSummary(result)
		"\n" println
	)

	/**
	 * Prints all reported failures from the given result.
	 */
	reportFailures := method(result,
		result failures foreach(fail, 
			"Failure: " print
			self reportTest(fail))
	)

	/**
	 * Prints all reported errors from the given result.
	 */
	reportErrors := method(result,
		result errors foreach(err, 
			"Error: " print
			self reportTest(err))
	)

	/**
	 * Print a report based on the given test information.
	 */
	reportTest := method(testInfo,
		"#{testInfo testProto} " interpolate print
		"#{testInfo testName}" interpolate println
		"#{testInfo msg}" interpolate println
	)

	/**
	 * Print a report summary based on the given result.
	 */
	reportSummary := method(result,
		"Tests: #{result testsRan}, " interpolate print

		failNum := result failures size
		"Failures: #{failNum}, " interpolate print

		errNum := result errors size
		"Errors: #{errNum}, " interpolate print

		time := result runStopTime - result runStartTime
		"Execution time: #{time asString(0, 3)}s" interpolate println)
)
