
/**
 * Aggregates the results from a test run.
 */
MiniTestResult := Object clone do (

	SUCCESS_MSG := "."
	FAILURE_MSG := "F"
	ERROR_MSG := "E"
	SKIP_MSG := "S"

	init := method(
		self failures := list()
		self errors := list()
		self testsRan := 0)

	/**
	 * Add a failure to the failure list.
	 *
	 * The argument _test_ is the proto that ran the tests and _err_ is the 
	 * cought exception.
	 */
	addFailure := method(test, err,
		self FAILURE_MSG print
		
		info := MiniTestInfo new(test, err coroutine backTraceString)
		self failures append(info)
		self incTestsRan
		info)

	/**
	 * Add an error to the error list.
	 *
	 * The argument _test_ is the proto that ran the tests and _err_ is the 
	 * cought exception.
	 */
	addError := method(test, err,
		self ERROR_MSG print
		
		info := MiniTestInfo new(test, err coroutine backTraceString)
		self errors append(info)
		self incTestsRan
		info)

	/**
	 * Add a successful test. 
	 */
	addSuccess := method(test,
		self SUCCESS_MSG print
		self incTestsRan)

	/**
	 * Increment the number of run tests.
	 */
	incTestsRan := method(
		self testsRan = self testsRan + 1)

	/**
	 * Called once at the start of the entire test run.
	 */
	startTestRun := method(
		self runStartTime := Date now asNumber)

	/**
	 * Called once after the entire test run.
	 */
	stopTestRun := method(
		self runStopTime := Date now asNumber)

	/**
	 * Called once before each test.
	 */
	startTest := method()

	/**
	 * Called once after each test.
	 */
	stopTest := method()
)


/**
 * Contains information about the execution of a specific test case.
 */
MiniTestInfo := Object clone do (

	init := method(
		self newSlot("testProto")
		self newSlot("testName")
		self newSlot("msg"))

	/**
	 * Return a new clone of MiniTestInfo with information from the test case
	 * proto _test_ and the string _msg_.
	 */
	new := method(test, msg,
		info := self clone
		info setTestProto(test type)
		info setTestName(test selector)
		info setMsg(msg)
		info)
)

