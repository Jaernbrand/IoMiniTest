
/**
 * Proto for creating tests. Clone MiniTestCase and implement the tests as 
 * methods in the newly cloned proto. Any slot matching the test slot pattern
 * will be assumed to be a test. The default slot pattern matches any slot 
 * beginning in _test_ and is defined in MiniTestLoader.
 */
MiniTestCase := Assert clone do (

	/**
	 * Selects which test to run.
	 */
	setSelector := method(selector,
		self selector := selector)

	/**
	 * Run the test specified by the selector.
	 */
	run := method(result, 
		result ?startTest
		self ?setUp
		e := try(
			self doString(selector)
			result addSuccess(self)) 

		e catch (AssertionException, 
			result addFailure(self, e)
		) catch (Exception, 
			result addError(self, e))
		self ?tearDown
		result ?stopTest)

	/**
	 * Returns the number of tests in the proto. This should always return 1,
	 * since each grandchild of MiniTestCase should only contain one test 
	 * selector. 
	 */
	testCount := method(1)
)


