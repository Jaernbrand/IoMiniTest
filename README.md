# IoMiniTest

IoMiniTest is a testing framework implemented in Io. It is based on Kent Beck's 
[Smalltalk testing framework](https://web.archive.org/web/20150315073817/http://www.xprogramming.com/testfram.htm).
IoMiniTest should be familiar to anyone already acquainted with JUnit or 
similar framework. Also see [http://iolanguage.org/](http://iolanguage.org/) 
for more information about the programming language Io.


## Dependencies

IoMiniTest depends on the Io Regex addon. This addon is likely included in the 
Io installation. In any case, the addon can be found in the source Io [repository](https://github.com/IoLanguage/io).
Easiest way to install it if it is missing is probalby to compile and reinstall 
Io.


## Installation

Note that installation is not required to be able to use IoMiniTest, Io just
need to be able to find the protos in `MiniTest`.

Run `build.io` with the argument `install` to install IoMiniTest for the Io 
instance used to run `build.io`. The framework will automatically be installed 
in `#{System ioPath}/packages`, which defaults to `$INSTALL_PREFIX/lib/io`. 
An executable, to allow execution from the command line, will also be installed 
in `$INSTALL_PREFIX/bin`. 

Change directory so that you are in the project root directory. Execute the
`build.io` and pass the argument `install`.
```
./build.io install
```

You can also run it via Io as follows.
```
io build.io install
```

The script `build.io` might have to be called via `sudo` depending on where Io 
is installed.
```
sudo ./build.io install
```

Add the installation directory to the environment variable `IOIMPORT`.
This is necessary if you want Io to find the IoMiniTest protos. The default 
installation location for \*nix should be `/usr/local/lib/io/packages`.
Add the following to `~/.bashrc`.
```
export IOIMPORT="$IOIMPORT:/usr/local/lib/io/packages"
```

For Winodws, add a new environment variable named `IOIMPORT` and set it to 
something such as the follwing example, where the path should point to where
IoMiniTest was installed. 
```
%IOIMPORT%;C:\IoLanguage\lib\io\packages
```
If `IOIMPORT` already is installed, then you can simply add the IoMiniTest 
installation path to the variable.

Test that IoMiniTest was successfully installed by running the version commmand.
```
IoMiniTest --version
```


## Uninstall

The script `build.io` also has a uninstall target that removes IoMiniTest from
the Io installation used to run `build.io`.

Change directory so that you are in the project root directory and execute
`build.io` with the argument `uninstall`.
```
./build.io uninstall
```

You can also run it via Io as follows.
```
io build.io uninstall
```

The script `build.io` might have to be called via `sudo` depending on where Io 
is installed.
```
sudo ./build.io uninstall
```

Finally, adjust the `IOIMPORT` environment variable so that it doesn't point 
to IoMiniTest's former installation directory.


## Run

IoMiniTest can be executed from the command line or from an Io script.


### Run from Command Line
If IoMiniTest was installed via `build.io` then it can be execute from the 
command line by typing `IoMiniTest`. 
```
IoMiniTest --version
```

The entry point of IoMiniTest is the script `MiniTest.io`. Tests can also be
run by executing `MiniTest.io` directly. However, `MiniTest.io` will likely not
be present in your path if installed via the `install` target in `build.io`.
Executing `MiniTest.io` is how you run IoMiniTest if the package wasn't 
installed via the build script. Also remember that Io has to be able to find 
the other protos in `MiniTest`.
```
./MiniTest.io --version
```
Or explicitly run with io.
```
io MiniTest.io --version
```
The same arguments is used regardless of whether `MiniTest.io` or `IoMiniTest` 
is executed. 


The default behaviour of IoMiniTest is to search for test cases recursively in
the current working directory and run all found tests. Simply call `IoMiniTest`
without arguments.
```
IoMiniTest 
```

To instead discover tests recursively in an other directory pass the path to
that directory as an argument. Note that you can only pass on directory to be
searched recursively.
```
IoMiniTest test
```
The example Will serch for tests in the directory `test` and all of its 
subdirectories and run all found tests.

A specific test case can be run by passing the path to it as argument.
```
IoMiniTest test/TestAsserts.io
```
The file extension is not necessary may be dropped since all test cases are 
assumed to be implemented in Io.
```
IoMiniTest test/TestAsserts
```
Multiple test cases may be specified by passing multiple paths.
```
IoMiniTest test/TestAsserts test/TestMiniTestCase
```

A simple help prompt is printed if `-h` or `--help` is passed.
```
IoMiniTest -h
IoMiniTest --help
```

The current version of IoMiniTest is printed if `--version` is passed.
```
IoMiniTest --version
```

### Run from Io

IoMiniTest can also be imported and run from an Io script. Io do of course have 
to know where to find the MiniTest protos. This can be achieved by setting 
`IOIMPORT` so that it points to the location where the MiniTest package is 
installed. See the heading [install](#install).

The proto `MiniTest` is hte main entry point for the framework and tests are 
run by calling the run method. Pass any arguments `MiniTest#run` as a list of
strings. An empty list will cause `MiniTest` to search for tests recursively 
in the current working directory.
```io
MiniTest run(list())
```
The absence of arguments will be treated the same way as if an empty list was 
passed.
```io
MiniTest run
```

Search for test cases recursively in the directory `test`. 
```io
MiniTest run(list("test"))
```

Run the test cases specified in the list.
```io
args := list("test/TestAsserts", "test/TestMiniTest")
MiniTest run(args))
```


## Create Tests

Clone the `MiniTestCase` proto and name the new clone so that it begins in 
`Test`. The `io` file where it resides have to be named the same as the new 
test case. `MiniTest` will automatically searches for `io` files beginning 
with `Test` when given a directory as argument.
```io
TestThing := MiniTestCase clone
```

All methods named according to the pattern `test*` will assumed to be tests. 
So to create a new test, add a new method to the test case proto and have the
name begin in `test`.
```io
TestThing testOnePlusOne := method(self assertEquals(1, 1))
```
All descendant of `MiniTestCase` have access to the assert methods defined in 
the proto `Assert`. 


### setUp

Runs once before each test. Implement this method in the test case proto if you 
have any test setup common amoung the tests in the test case. 
```io
TestThing setUp := method( /* Common test setup code */ )
```

### tearDown

Runs once after each test. Implement this method in the test case proto if you 
have any test cleanup common amoung the tests in the test case.
```io
TestThing tearDown := method( /* Common tear down code */ )
```

### preSetUp

Runs only once for each TestCase proto before the first `setUp`. The method 
will be run in the context of the test case proto. Use it if you have anything
that only need to run once for the entire test case proto.
```io
TestThing preSetUp := method( /* Common setup to run once before all tests */ )
```


### postTearDown

Runs only once for each TestCase proto after the last `tearDown`. The method 
will be run in the context of the test case proto. Use it if you have to clean 
up anything once for the entire test case proto.
```io
TestThing postTearDown := method( /* Common clean up to run once after all tests */ )
```

