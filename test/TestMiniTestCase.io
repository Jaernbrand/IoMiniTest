
TestMiniTestCase := MiniTestCase clone do (

	testRun := method(
		testCase := MiniTestCase clone
		selector := "testFoo"
		testCase setSelector(selector)

		protoMethods := list("preSetUp", "postTearDown")
		protoMethods foreach(m, 
			MiniTestCase doString("#{m} := method()" interpolate))

		methods := list("setUp", selector, "tearDown")
		methods foreach(m, 
			testCase doString("#{m} := method()" interpolate))

		interceptor := testCase clone do (

			slotHistory := list()

			interceptSlots := method(slots,
				slots foreach(slot, 
					self doString("""#{slot} := method(
						slotName := call message name
						self slotHistory append(slotName)	
						resend)""" interpolate)))
		)

		interceptor interceptSlots(methods)

		suite := MiniTestSuite clone
		suite addTest(interceptor)

		result := MiniTestResult clone
		self silencePrint
		suite run(result)
		self restorePrint

		orcaleMethods := list() appendSeq(protoMethods slice(0, 1),
			methods,
			protoMethods slice(1))
		self assertEquals(methods, interceptor slotHistory)
	)

	silencePrint := method(
		self oldPrint := Sequence getSlot("print")
		Sequence print := method()

		self oldPrintln := Sequence getSlot("println")
		Sequence println := method())

	restorePrint := method(
		Sequence print := self getSlot("oldPrint")
		Sequence println := self getSlot("oldPrintln"))

)

