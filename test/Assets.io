
Assets := Object clone do (

	rootDir := "TmpTestAssets"

	clean := method(
		(Directory exists(self rootDir)) ifTrue (
			root := Directory with(self rootDir)
			root remove)
	)

	create := method(
		self _create(self rootDir, self rootDirFiles, 1, 3))

	_create := method(dirName, files, counter, dirDepth,
		if (counter > dirDepth, return)

		self createDir(dirName, files)
		
		subName := list(dirName, "subdir#{counter}" interpolate) join("/")
		files = self subdirFiles(counter)
		self _create(subName, files, counter+1, dirDepth))

	createDir := method(dir, files,
		Directory createSubdirectory(dir)
		files foreach(currFile, 
			filePath := list(dir, currFile at(0)) join("/")
			f := File open(filePath)
			ex := try( f write(currFile at(1)) )
			f close
			ex pass)
		)

	rootDirFiles := method(
		files := list()

		files append(list("TestFile0.io", 
		"""
		TestFile0 := MiniTestCase clone

		TestFile0 testMethod := method()

		TestFile0 testMethodFailure := method(self fail)

		TestFile0 testMethodError := method(Exception raise)

		TestFile0 nonTestMethod := method()
		"""))

		files append(list("NonDefaultTestFile.io", 
		"""
		NonDefaultTestFile := MiniTestCase clone

		NonDefaultTestFile testMethod := method()

		NonDefaultTestFile nonTestMethod := method()
		"""))

		files append(list("TestNoFileType", 
		"""
		NonDefaultTestFile := MiniTestCase clone

		NonDefaultTestFile testMethod := method()

		NonDefaultTestFile nonTestMethod := method()
		"""))

		files append(list("TestWrongFileType.py", 
		"""
		NonDefaultTestFile := MiniTestCase clone

		NonDefaultTestFile testMethod := method()

		NonDefaultTestFile nonTestMethod := method()
		"""))

		files)

	subdirFiles := method(num,
		files := list()

		files append(list("TestFile#{num}.io" interpolate, 
		"""
		TestFile#{num} := MiniTestCase clone

		TestFile#{num} testMethod := method()

		TestFile#{num} testMethodFailure := method(self fail)

		TestFile#{num} testMethodError := method(Exception raise)

		TestFile#{num} nonTestMethod := method()
		""" interpolate))

		files)
)



