
TestAssert := MiniTestCase clone do(

	testAssertEquals := method(
		Assert assertEquals(1, 1)

		defaultMsg := "'1' != '2'"
		e := try(Assert assertEquals(1,2))
		self doFaildAssertTest(e, defaultMsg)
		
		customMsg := "1 did not equal 2"
		e := try(Assert assertEquals(1, 2, customMsg))
		self doFaildAssertTest(e, customMsg)
	)
		
	testAssertNotEquals := method(
		Assert assertNotEquals(1, 2)

		defaultMsg := "'1' == '1'"
		e := try(Assert assertNotEquals(1,1))
		self doFaildAssertTest(e, defaultMsg)
		
		customMsg := "1 was equal to 1"
		e := try(Assert assertNotEquals(1, 1, customMsg))
		self doFaildAssertTest(e, customMsg)
	)

	testAssertAlmostEquals := method(
		Assert assertAlmostEquals(1, 1.5, 0.5)

		defaultMsg := "'#{1.1}' != '2' within delta '#{0.5}'" interpolate
		e := try(Assert assertAlmostEquals(1.1, 2, 0.5))
		self doFaildAssertTest(e, defaultMsg)
		
		customMsg := "1 did not almost equal 2 with delta 0.5"
		e := try(Assert assertAlmostEquals(1, 2, 0.5, customMsg))
		self doFaildAssertTest(e, customMsg)
	)

	testAssertNotAlmostEquals := method(
		Assert assertNotAlmostEquals(1, 1.6, 0.5)

		defaultMsg := "'#{2.1}' == '2' within delta '#{0.5}'" interpolate
		e := try(Assert assertNotAlmostEquals(2.1, 2, 0.5))
		self doFaildAssertTest(e, defaultMsg)
		
		customMsg := "1.6 was equal to 2 with delta 0.5"
		e := try(Assert assertNotAlmostEquals(1.6, 2, 0.5, customMsg))
		self doFaildAssertTest(e, customMsg)
	)

	testAssertNil := method(
		Assert assertNil(nil)

		defaultMsg := "'' != 'nil'"
		e := try(Assert assertNil(""))
		self doFaildAssertTest(e, defaultMsg)
		
		customMsg := "The given object was not nil"
		e := try(Assert assertNil("", customMsg))
		self doFaildAssertTest(e, customMsg)
	)

	testAssertNotNil := method(
		Assert assertNotNil("")

		defaultMsg := "'nil' == 'nil'"
		e := try(Assert assertNotNil(nil))
		self doFaildAssertTest(e, defaultMsg)
		
		customMsg := "The given object was nil"
		e := try(Assert assertNotNil(nil, customMsg))
		self doFaildAssertTest(e, customMsg)
	)

	testAssertTrue := method(
		Assert assertTrue(true)

		defaultMsg := "'false' != 'true'"
		e := try(Assert assertTrue(false))
		self doFaildAssertTest(e, defaultMsg)
		
		customMsg := "The assert was false, but expected true"
		e := try(Assert assertTrue(false, customMsg))
		self doFaildAssertTest(e, customMsg)
	)

	testAssertFalse := method(
		Assert assertFalse(false)

		defaultMsg := "'true' != 'false'"
		e := try(Assert assertFalse(true))
		self doFaildAssertTest(e, defaultMsg)
		
		customMsg := "The assert was true, but expected false"
		e := try(Assert assertFalse(true, customMsg))
		self doFaildAssertTest(e, customMsg)
	)

	testAssertFail := method(
		defaultMsg := "Fail (no reason given)"
		e := try(Assert fail)
		self doFaildAssertTest(e, defaultMsg)

		customMsg := "The reason for failure given here"
		e := try(Assert fail(customMsg))
		self doFaildAssertTest(e, customMsg)
	)

	doFaildAssertTest := method(e, msg,
		(e == nil) ifTrue (self fail("Expected exception to be raised, but none were raised"))

		e catch(AssertionException,
			self assertEquals(msg, e error message)
		) pass
	)

)

