
TestMiniTest := MiniTestCase clone do (

	setUp := method(
		Assets create

		self oldPrint := Sequence getSlot("print")
		Sequence print := method()

		self oldPrintln := Sequence getSlot("println")
		Sequence println := method()
	)

	tearDown := method(
		Sequence print := self getSlot("oldPrint")
		Sequence println := self getSlot("oldPrintln")
		
		Assets clean
	)

	testRunSpecifiedDir := method(
		dir := "TmpTestAssets"
		args := list(dir)
		result := MiniTest run(args)

		ranOracle := 9
		self assertEquals(ranOracle, result testsRan)

		failureOracle := 3
		self assertEquals(failureOracle, result failures size)

		errorOracle := 3
		self assertEquals(errorOracle, result errors size)
	)

	testRunSpecifiedTestCase := method(
		casePath := "TmpTestAssets/TestFile0"
		args := list(casePath)
		result := MiniTest run(args)
		
		ranOracle := 3
		self assertEquals(ranOracle, result testsRan)

		failureOracle := 1
		self assertEquals(failureOracle, result failures size)

		errorOracle := 1
		self assertEquals(errorOracle, result errors size)
	)

)

